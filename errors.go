package jwtgoauth

import (
	"encoding/json"
	"errors"
	"net/http"
)

var (
	ErrorInvalidToGenerateToken = errors.New("Invalid to generate token")
	ErrorTokenMalformed         = errors.New("That's not even a token")
	ErrorTokenIsNotValid        = errors.New("Token is not valid")
	ErrorInternalServer         = errors.New("Something went wrong")
	ErrorTokenHasExpired        = errors.New("Token has expired")
	ErrorTokenNotFound          = errors.New("Token not found")
)

func WriteError(w http.ResponseWriter, err error) {
	var resp interface{}
	code := http.StatusInternalServerError

	resp = ResponseBody{
		Message: err.Error(),
		Meta: ResponseMeta{
			HTTPStatus: code,
		},
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(resp)
}
