package jwtgoauth

import (
	"errors"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

func GenerateToken(payload JwtGoAuthPayload, keys JWTKey) (string, error) {
	expiryAt := time.Now().Add(time.Duration(payload.ExpiryTime) * time.Minute)

	claims := Claims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    keys.Issuer,
			ExpiresAt: expiryAt.Unix(),
		},
		UserID:     payload.UserID,
		Email:      payload.Email,
		Name:       payload.Name,
		Username:   payload.Username,
		Custom:     payload.Custom,
		ExpiryAt:   expiryAt,
		ExpiryTime: payload.ExpiryTime,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	ss, err := token.SignedString([]byte(keys.SigningKey))
	if err != nil {
		return ss, ErrorInvalidToGenerateToken
	}

	return ss, nil
}

func OpenToken(token string, keys JWTKey) (JwtGoAuthPayload, error) {
	var result JwtGoAuthPayload

	tokenParse, err := jwt.ParseWithClaims(token, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(keys.SigningKey), nil
	})

	if errors.Is(err, jwt.ErrTokenMalformed) {
		return result, ErrorTokenMalformed

	} else if errors.Is(err, jwt.ErrTokenNotValidYet) {
		return result, ErrorTokenIsNotValid

	} else if errors.Is(err, jwt.ErrTokenExpired) {
		return result, ErrorTokenHasExpired
	}

	c, ok := tokenParse.Claims.(*Claims)
	if !ok || !tokenParse.Valid {
		return result, err
	}

	result = JwtGoAuthPayload{
		UserID:     c.UserID,
		ExpiryAt:   c.ExpiryAt,
		ExpiryTime: c.ExpiryTime,
		Email:      c.Email,
		Name:       c.Name,
		Username:   c.Username,
		Custom:     c.Custom,
	}

	return result, nil
}
