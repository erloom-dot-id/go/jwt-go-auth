package jwtgoauth

import (
	"time"

	"github.com/golang-jwt/jwt/v4"
)

type JwtGoAuthPayload struct {
	UserID     uint                   `json:"user_id"`
	ExpiryTime int                    `json:"expiry_time"` // In Minute
	ExpiryAt   time.Time              `json:"expiry_at"`
	Email      string                 `json:"email"`
	Name       string                 `json:"name"`
	Username   string                 `json:"username"`
	Custom     map[string]interface{} `json:"custom"`
}

type Claims struct {
	jwt.StandardClaims
	UserID     uint                   `json:"user_id"`
	ExpiryTime int                    `json:"expiry_time"`
	ExpiryAt   time.Time              `json:"expiry_at"`
	Email      string                 `json:"email"`
	Name       string                 `json:"name"`
	Username   string                 `json:"username"`
	Custom     map[string]interface{} `json:"custom"`
}

type JWTKey struct {
	Issuer     string `json:"issuer"`
	SigningKey string `json:"signing_key"`
}

type ResponseBody struct {
	Data    interface{}  `json:"data,omitempty"`
	Message string       `json:"message,omitempty"`
	Meta    ResponseMeta `json:"meta"`
}

type ResponseMeta struct {
	HTTPStatus int   `json:"http_status"`
	Total      *uint `json:"total,omitempty"`
	Offset     *uint `json:"offset,omitempty"`
	Limit      *uint `json:"limit,omitempty"`
	Page       *uint `json:"page,omitempty"`
}
