package jwtgoauth

import (
	"context"
	"net/http"
	"strings"

	"github.com/felixge/httpsnoop"
	"github.com/google/uuid"
	"gitlab.com/erloom-dot-id/go/jwt-go-auth/logger"
)

func PublicMiddleware(next http.Handler) http.Handler {
	return standardMiddleware(next)
}

func AuthMiddleware(next http.Handler, keys JWTKey) http.Handler {
	return standardMiddleware(validationToken(next, keys))
}

func validationToken(next http.Handler, keys JWTKey) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		authHeader := r.Header.Get("Authorization")
		splitToken := strings.Split(authHeader, "Bearer ")

		if len(splitToken) < 2 {
			WriteError(w, ErrorTokenNotFound)
			return
		}

		accessToken := splitToken[1]

		payload, err := OpenToken(accessToken, keys)
		if err != nil {
			return
		}

		ctx := context.WithValue(r.Context(), "JWTGoAuthPayload", payload)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func standardMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		ctx := context.WithValue(r.Context(), "X-Request-ID", uuid.NewString())

		m := httpsnoop.CaptureMetrics(panicMiddleware(next), w, r.WithContext(ctx))

		logger.Info(ctx, "http api request", map[string]interface{}{
			"method":   r.Method,
			"path":     r.URL,
			"status":   m.Code,
			"duration": m.Duration.Milliseconds(),
			"package":  "JWT Go Auth",
		})
	})
}

func panicMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if rec := recover(); rec != nil {
				logger.Error(r.Context(), "panic occured", map[string]interface{}{
					"error":   rec,
					"package": "JWT Go Auth",
				})
				WriteError(w, ErrorInternalServer)
			}
		}()

		next.ServeHTTP(w, r)
	})
}
