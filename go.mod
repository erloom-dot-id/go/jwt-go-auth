module gitlab.com/erloom-dot-id/go/jwt-go-auth

go 1.18

require (
	github.com/felixge/httpsnoop v1.0.3
	github.com/golang-jwt/jwt/v4 v4.4.3
	github.com/google/uuid v1.3.0
	go.uber.org/zap v1.24.0
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
