package main

import (
	"fmt"
	"log"

	jwtgoauth "gitlab.com/erloom-dot-id/go/jwt-go-auth"
)

var (
	myUniqueString = "asdasd"
	mySignKey      = "signKey"
)

func main() {
	payload := jwtgoauth.JwtGoAuthPayload{
		UserID:     123,
		ExpiryTime: 1,
		Email:      "email@email.com",
		Username:   "username",
		Name:       "name",
	}

	myKey := jwtgoauth.JWTKey{
		Issuer:     "asdasd",
		SigningKey: "signingKey",
	}

	res, err := jwtgoauth.GenerateToken(payload, myKey)
	if err != nil {
		log.Println(err)
		return
	}

	fmt.Println(res)

	resOpen, err := jwtgoauth.OpenToken(res, myKey)
	if err != nil {
		log.Println(err)
		return
	}

	fmt.Println(resOpen)
	return
}
